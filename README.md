# Flectra Community / l10n-brazil

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[l10n_br_base](l10n_br_base/) | 2.0.2.2.0| Customization of base module for implementations in Brazil.
[l10n_br_stock](l10n_br_stock/) | 2.0.2.0.0| Brazilian Localization Warehouse
[l10n_br_purchase](l10n_br_purchase/) | 2.0.1.0.0| Brazilian Localization Purchase
[l10n_br_portal](l10n_br_portal/) | 2.0.1.0.0|         Campos Brasileiros no Portal
[l10n_br_currency_rate_update](l10n_br_currency_rate_update/) | 2.0.1.0.0| Update exchange rates using OCA modules for Brazil
[l10n_br_fiscal](l10n_br_fiscal/) | 2.0.7.3.0| Brazilian fiscal core module.
[l10n_br_account_due_list](l10n_br_account_due_list/) | 2.0.1.0.0| Brazilian Account Due List
[l10n_br_resource](l10n_br_resource/) | 2.0.1.0.0|         This module extend core resource to create important brazilian        informations. Define a Brazilian calendar and some tools to compute        dates used in financial and payroll modules
[l10n_br_nfe](l10n_br_nfe/) | 2.0.2.1.0| Brazilian Eletronic Invoice NF-e .
[spec_driven_model](spec_driven_model/) | 2.0.1.0.1|         Tools for specifications driven mixins (from xsd for instance)
[l10n_br_coa_simple](l10n_br_coa_simple/) | 2.0.2.1.0| Plano de Contas ITG 1000 para Microempresas e Empresa de Pequeno Porte
[l10n_br_coa](l10n_br_coa/) | 2.0.3.2.0|         Base do Planos de Contas brasileiros
[l10n_br_sale](l10n_br_sale/) | 2.0.1.0.1| Brazilian Localization Sale
[l10n_br_contract](l10n_br_contract/) | 2.0.1.0.0|         Customization of Contract module for implementations in Brazil.
[l10n_br_zip](l10n_br_zip/) | 2.0.1.0.0| Brazilian Localisation ZIP Codes
[l10n_br_mis_report](l10n_br_mis_report/) | 2.0.1.0.0|         Templates de relatórios contábeis brasileiros: Balanço Patrimonial e DRE
[l10n_br_coa_generic](l10n_br_coa_generic/) | 2.0.2.1.0| Plano de Contas para empresas do Regime normal        (Micro e pequenas empresas)
[l10n_br_account_payment_brcobranca](l10n_br_account_payment_brcobranca/) | 2.0.1.0.0| L10n Br Account Payment BRCobranca
[l10n_br_crm](l10n_br_crm/) | 2.0.1.0.0| Brazilian Localization CRM
[l10n_br_website_sale](l10n_br_website_sale/) | 2.0.1.0.0|         Website sale localização brasileira.
[l10n_br_account](l10n_br_account/) | 2.0.1.2.0| Brazilian Localization Account
[l10n_br_account_payment_order](l10n_br_account_payment_order/) | 2.0.1.1.0| Brazilian Payment Order
[l10n_br_nfe_spec](l10n_br_nfe_spec/) | 2.0.1.0.0| nfe spec
[l10n_br_hr](l10n_br_hr/) | 2.0.1.0.0| Brazilian Localization HR
[l10n_br_nfse](l10n_br_nfse/) | 2.0.1.9.1|         NFS-e


