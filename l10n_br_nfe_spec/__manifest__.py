{
    "name": "nfe spec",
    "version": "2.0.1.0.0",
    "author": "Akretion, Odoo Community Association (OCA)",
    "license": "LGPL-3",
    "category": "Accounting",
    "summary": "nfe spec",
    "depends": ["base"],
    "external_dependencies": {
        "python": [
            "nfelib",  # (only for tests)
        ],
    },
    "installable": True,
    "application": False,
    "development_status": "Production/Stable",
    "maintainers": ["rvalyi"],
    "website": "https://gitlab.com/flectra-community/l10n-brazil",
}
